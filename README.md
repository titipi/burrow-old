# Burrow
**interdependent pad backup**

## Description
Burrow is a rudimentary script for archiving interdependent etherpad collections that might be distributed over multiple servers and etherpad installs. It is aimed at [padologists](https://march.international/constant-padology/) that don't have access to the server that hosts their etherpad files or that work across different groups with each their own etherpad installations. 

Pads listed on any pad specified in burrow.sh will be saved as textfiles at a given daily interval (when using crontab), off-line or on-line. 

Burrow offers padologists a simple back-up plan for when pads are down or when needing to work off-line. Burrow protects against the unwanted loss of expired pads and lessens the dependency on always being connected. It also goes against the tendency to centralize installations, and towards a non-sovereign infrastructure approach. 

## Usage

In burrow.sh, add the URl of a main pad with links to other pads that will need to be archived. Add export/txt to the end of the URl.
On this index pad, use a new line for each link that you want to be archived. 

`INDEX_PAD="https://path-to-index-pad/export/txt" `

Optional: change the amount of days files should be kept (not implemented yet)

`RETAIN="90"`

Save burrow.sh in a folder. Open a terminal, move into the folder using cd and run the script manually with: 

`$ bash burrow.sh`

or login to a server, save the script in a folder and set up a scheduled job with crontab:

`$ crontab -e`

Then, open the crontab file in an editor and add the following (add the full path to the burrow.sh script):

`0 0 * * * /full/path/to/script/burrow.sh`

This will run Burrow every day at midnight: for other frequencies see the crontab manual.

## Acknowledgment

Burrow is named after the tunnels that rodents and other small animals dig. These tunnels function at the same time as conduit, habitat, temporary refuge or simply as a byproduct of their movement through matter.

Bash scripting by Aggeliki Diakrousi, hello etherdump! Written in the context of [The Social Life of XG](https://chanse.org/solixg/).

## TO-DO

* Check changes
* Add option to remove downloaded files after RETAIN days
* Save pad versions in a folder
* Integrate crontab in script (?)
* Fallback when no internet, pads not available
* error messages

## License

CC4r https://constantvzw.org/wefts/cc4r.en.html

