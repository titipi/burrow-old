#!/bin/bash

# Burrow is a rudimentary solution for archiving interdependent etherpad collections that are distributed over multiple servers and etherpad installs, and for padologist that don't have access to the server that hosts their etherpad files.

# Sources and instructions: https://gitlab.constantvzw.org/titipi/burrow

# SETTINGS

# Address of the main pad with links to other pads that will need to be archived. 
INDEX_PAD="https://path-to-archive-pad/export/txt" 

# How many days should Burrow keep files?
RETAIN="90"

string="/export/txt"

# LAUNCH BURROW

# Download INDEX_PAD
wget $INDEX_PAD

# Rename INDEX_PAD into tmp-links-all.txt
mv txt tmp-links-all.txt

# Remove lines that are not links
grep https tmp-links-all.txt > tmp-links-less.txt

# Append /export/txt at the end of each link
cat tmp-links-less.txt | while read line; do echo ${line}${string}>>tmp-links.txt; done

# Download all files mentioned in non-sovereign-archive.txt
cat tmp-links.txt | while read line; do wget --content-disposition ${line}; done 

rm tmp-links*

# Add date to filenames
for file in *.txt 

do

  mv "$file" "${file%.txt}_$(date +"%Y-%m-%d").txt"

done

# Delete files that have been archived more than RETAIN (we have to check this first)
# find *txt -mindepth 1 -mtime +$RETAIN -delete

